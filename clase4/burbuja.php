<?php

/* 
 * Archivo programado por Luis Sepúlveda (luinux).
 * Usted puede reutilizar el codigo dando los creditos 
 * al autor. http://www.luinux.com
 */

$nums = array(9, 20, 5, 1, 40);

echo '<h2>Numeros del array en orden original</h2>';
foreach ($nums as $num) {
    echo $num . '<br>'; 
}


for ($i = 0; $i < (count($nums) - 1); $i++) {
    for ($j = 0; $j < (count($nums) - 1); $j++) {
        if ($nums[$j] > $nums[$j + 1]) {
            $aux = $nums[$j];
            $nums[$j] = $nums[$j + 1];
            $nums[$j + 1] = $aux;
        }
    }
}


echo '<h2>Numeros del array en ordenados ascendentemente</h2>';
foreach ($nums as $num) {
    echo $num . '<br>';
}