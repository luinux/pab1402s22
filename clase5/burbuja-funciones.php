<?php

/* 
 * Archivo programado por el grupo entre Tamara Paillao y Luis Sepúlveda.
 * Usted puede reutilizar el codigo dando los creditos 
 */

$numeros = array(9, 20, 5, 1, 40);

function ordenar($nums) // la funcion recibe un array de numeros
{
    for ($i = 0; $i < count($nums) - 1; $i++)
    {
        for ($j = 0; $j < count($nums) - 1; $j++)
        {
            if ($nums[$j] > $nums[$j + 1]) // si es mayor que el siguiente entra
            {
                $aux          = $nums[$j]; // guarda en variable $aux el valor de la posicion
                $nums[$j]     = $nums[$j + 1]; // setea el valor con la posicion con el valor de la siguiente
                $nums[$j + 1] = $aux; // la posicion siguiente la setea con el valor que guardamos en $aux
            }
        }
    }
    return $nums; // retorna un array ordenado
}

function imprimir($arreglo)
{
    foreach ($arreglo as $num) // recorre el array recibido y lo imprime linea a linea
    {
        echo $num . '<br>';
    }
}

echo '<h2>Numeros del array en orden original</h2>';
imprimir($numeros);

echo '<h2>Numeros del array en ordenados ascendentemente</h2>';
imprimir(ordenar($numeros));
